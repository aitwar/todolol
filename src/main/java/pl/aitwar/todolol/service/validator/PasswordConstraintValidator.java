package pl.aitwar.todolol.service.validator;

import com.nulabinc.zxcvbn.Strength;
import com.nulabinc.zxcvbn.Zxcvbn;
import pl.aitwar.todolol.service.constraint.ValidPassword;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordConstraintValidator implements ConstraintValidator<ValidPassword, String> {
    private Zxcvbn zxcvbn;
    private Integer threshold;

    @Override
    public void initialize(ValidPassword arg0) {
        zxcvbn = new Zxcvbn();
        threshold = arg0.threshold();
    }
 
    @Override
    public boolean isValid(String password, ConstraintValidatorContext context) {
        Strength result = zxcvbn.measure(password);
        return result.getScore() >= threshold;
    }
}