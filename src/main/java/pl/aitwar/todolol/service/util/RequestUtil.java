package pl.aitwar.todolol.service.util;

import javax.servlet.http.HttpServletRequest;

public final class RequestUtil {
    public static String getRequestIP(HttpServletRequest request) {
        String xfHeader = request.getHeader("X-Forwarded-For");
        if (xfHeader == null){
            return request.getRemoteAddr();
        }
        return xfHeader.split(",")[0];
    }
}
