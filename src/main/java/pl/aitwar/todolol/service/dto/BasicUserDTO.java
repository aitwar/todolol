package pl.aitwar.todolol.service.dto;

import pl.aitwar.todolol.domain.User;

public class BasicUserDTO {
    private Long id;
    private String username;

    public BasicUserDTO(User user) {
        this.id = user.getId();
        this.username = user.getLogin();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
