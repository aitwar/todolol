package pl.aitwar.todolol.service.dto;

import java.time.Instant;
import java.util.Set;

public class TaskDTO {
    private Long id;
    private Long ownerId;
    private String title;
    private String content;
    private String type;
    private Set<Long> recipientsIds;
    private Instant createdDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Set<Long> getRecipientsIds() {
        return recipientsIds;
    }

    public void setRecipientsIds(Set<Long> recipientsIds) {
        this.recipientsIds = recipientsIds;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }
}
