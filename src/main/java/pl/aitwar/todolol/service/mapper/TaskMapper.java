package pl.aitwar.todolol.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import pl.aitwar.todolol.domain.Task;
import pl.aitwar.todolol.domain.User;
import pl.aitwar.todolol.service.dto.TaskDTO;

import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Mapper for the entity Task and its DTO TaskDTO.
 */
@Mapper(componentModel = "spring", uses={ UserMapper.class })
public interface TaskMapper extends EntityMapper<TaskDTO, Task> {

    @Mapping(source = "owner.id", target = "ownerId")
    @Mapping(source = "recipients", target = "recipientsIds")
    TaskDTO toDto(Task task);

    @Mapping(source = "ownerId", target = "owner")
    @Mapping(source = "recipientsIds", target = "recipients")
    Task toEntity(TaskDTO taskDTO);

    default Task fromId(Long id) {
        if (id == null) {
            return null;
        }
        Task task = new Task();
        task.setId(id);
        return task;
    }

    default Set<Long> map(Set<User> value) {
        if(value == null) {
            return Collections.emptySet();
        }

        return value.stream().map(User::getId).collect(Collectors.toSet());
    }
}
