package pl.aitwar.todolol.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.aitwar.todolol.domain.Task;
import pl.aitwar.todolol.repository.TaskRepository;
import pl.aitwar.todolol.service.dto.TaskDTO;
import pl.aitwar.todolol.service.mapper.TaskMapper;

import java.util.Optional;

/**
 * Service Implementation for managing Task.
 */

@Service
@Transactional
public class TaskService {
    private final Logger log = LoggerFactory.getLogger(TaskService.class);
    private final TaskRepository taskRepository;
    private final TaskMapper taskMapper;
    private final UserService userService;

    public TaskService(TaskRepository taskRepository, TaskMapper taskMapper, UserService userService) {
        this.taskRepository = taskRepository;
        this.taskMapper = taskMapper;
        this.userService = userService;
    }

    /**
     * Save a task.
     *
     * @param taskDTO the entity to save
     * @return the persisted entity
     */
    public TaskDTO save(TaskDTO taskDTO) {
        log.debug("Request to save Task : {}", taskDTO);

        return userService.getUserWithAuthorities()
            .map(user -> {
                Task task = taskMapper.toEntity(taskDTO);
                task.setOwner(user);
                task = taskRepository.save(task);
                return taskMapper.toDto(task);
            })
            .orElse(null);
    }

    /**
     * Get all the tasks.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<TaskDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Tasks");
        return taskRepository.findAll(pageable)
            .map(taskMapper::toDto);
    }

    /**
     * Get all tasks visible by user
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<TaskDTO> findAllVisibleByUser(Pageable pageable) {
        log.debug("Request to get all Tasks visible by current user");

        return userService.getUserWithAuthorities().map(user ->
            taskRepository.findAllVisibleByUser(pageable, user).map(taskMapper::toDto)
        ).orElseGet(Page::empty);
    }


    /**
     * Get one task by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<TaskDTO> findOne(Long id) {
        log.debug("Request to get Task : {}", id);

        return userService.getUserWithAuthorities()
            .flatMap(user -> taskRepository.findById(id)
                    .filter(task -> task.getOwner()
                    .equals(user)).map(taskMapper::toDto)
            );
    }

    /**
     * Delete the task by id.
     *
     * @param id the id of the entity
     */
    public boolean delete(Long id) {
        log.debug("Request to delete Task : {}", id);

        return userService.getUserWithAuthorities()
            .filter(user1 ->
                taskRepository.findById(id)
                .filter(task -> task.getOwner().equals(user1))
                .map(task -> {
                    taskRepository.delete(task);
                    return task;
                })
                .isPresent())
            .isPresent();
    }
}
