package pl.aitwar.todolol.api.errors;

import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

public class LoginTimeoutException extends AbstractThrowableProblem {
    private static final long serialVersionUID = 1L;

    public LoginTimeoutException() {
        super(ErrorConstants.DEFAULT_TYPE, "Login timeout", Status.BAD_REQUEST);
    }
}
