package pl.aitwar.todolol.api.errors;

import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

public class LoginBanException extends AbstractThrowableProblem {
    private static final long serialVersionUID = 1L;

    public LoginBanException() {
        super(ErrorConstants.DEFAULT_TYPE, "Login ban", Status.BAD_REQUEST);
    }
}
