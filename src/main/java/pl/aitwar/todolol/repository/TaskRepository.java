package pl.aitwar.todolol.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.aitwar.todolol.domain.Task;
import pl.aitwar.todolol.domain.User;


/**
 * Spring Data repository for the Task entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {
    @Query(value = "SELECT * FROM task t" +
            " WHERE t.owner_id = ?1 OR t.type = 'ALL' OR (t.type = 'SELECTED' AND EXISTS (" +
            " SELECT 1 FROM task_recipients WHERE task_id = t.id AND recipients_id = ?1" +
            " )) ORDER BY ?#{#pageable}", nativeQuery = true)
    Page<Task> findAllVisibleByUser(Pageable pageable, User user);
}
