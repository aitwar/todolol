package pl.aitwar.todolol.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.aitwar.todolol.domain.Authority;

/**
 * Spring Data JPA repository for the Authority entity.
 */
public interface AuthorityRepository extends JpaRepository<Authority, String> {
}
