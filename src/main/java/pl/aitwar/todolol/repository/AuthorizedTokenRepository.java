package pl.aitwar.todolol.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.aitwar.todolol.domain.AuthorizedToken;

/**
 * Spring Data repository for the Authorized Token entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AuthorizedTokenRepository extends JpaRepository<AuthorizedToken, String> {
}
