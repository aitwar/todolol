package pl.aitwar.todolol.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import pl.aitwar.todolol.domain.enumeration.TaskVisibility;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A Task.
 */
@Entity
@Table(name = "task")
public class Task extends AbstractAuditingEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "title")
    private String title;

    @NotNull
    @Size(min = 1, max = 256)
    @Column(name = "content", length = 256, nullable = false)
    private String content;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = false)
    private TaskVisibility type;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("")
    private User owner;

    @ManyToMany
    @JsonIgnore
    private Set<User> recipients = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public Task title(String title) {
        this.title = title;
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public Task content(String content) {
        this.content = content;
        return this;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public TaskVisibility getType() {
        return type;
    }

    public Task type(TaskVisibility type) {
        this.type = type;
        return this;
    }

    public void setType(TaskVisibility type) {
        this.type = type;
    }

    public User getOwner() {
        return owner;
    }

    public Task owner(User owner) {
        this.owner = owner;
        return this;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public Set<User> getRecipients() {
        return recipients;
    }

    public Task recipients(Set<User> recipients) {
        this.recipients = recipients;
        return this;
    }

    public Task addRecipients(User recipients) {
        this.recipients.add(recipients);
        return this;
    }

    public Task removeRecipients(User recipients) {
        this.recipients.remove(recipients);
        return this;
    }

    public void setRecipients(Set<User> recipients) {
        this.recipients = recipients;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Task task = (Task) o;
        if (task.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), task.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Task{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", content='" + getContent() + "'" +
            ", type='" + getType() + "'" +
            "}";
    }
}
