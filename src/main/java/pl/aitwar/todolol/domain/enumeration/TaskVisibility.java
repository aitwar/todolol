package pl.aitwar.todolol.domain.enumeration;

/**
 * The TaskVisibility enumeration.
 */
public enum TaskVisibility {
    ALL, SELECTED
}
