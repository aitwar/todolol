package pl.aitwar.todolol.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * An authorized tokenHash used by Spring Security.
 */
@Entity
@Table(name = "authorized_token")
public class AuthorizedToken implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    private String tokenHash;

    private String username;

    @CreatedDate
    @Column(name = "created_date", updatable = false)
    @JsonIgnore
    private Instant createdDate = Instant.now();

    public AuthorizedToken() {}

    public AuthorizedToken(String token, String user) {
        this.tokenHash = token;
        this.username = user;
    }

    public String getTokenHash() {
        return tokenHash;
    }

    public void setTokenHash(String tokenHash) {
        this.tokenHash = tokenHash;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AuthorizedToken that = (AuthorizedToken) o;
        return Objects.equals(tokenHash, that.tokenHash);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tokenHash);
    }

    @Override
    public String toString() {
        return "AuthorizedToken{" +
                "tokenHash='" + tokenHash + '\'' +
                ", username='" + username + '\'' +
                "}";
    }
}
